<?php

class ColorRGB{
    
    private $red;
    private $green;
    private $blue;
    private $opacity;
    
    public function __construct(int $blue, int $green, int $red, $opacity = 0)
    {
        $this->setBlue($blue);
        $this->setGreen($green);
        $this->setRed($red);
        $this->setOpacity($opacity);
    }

    public function mix(ColorRGB $new){
        $this->red = ($this->red + $new->getRed())/2;
        $this->blue = ($this->blue + $new->getBlue())/2;
        $this->green = ($this->green + $new->getGreen())/2;
    }
    
    static function random(bool $enableOpacity)
    {
        return new ColorRGB(random_int(0,255), random_int(0,255), random_int(0,255), $enableOpacity ?  round(lcg_value(),2) : 1.00);
    }
    
    private function validateColor(int $color) : int
    {
        if(0 <= $color || $color <= 255) return $color;
        throw new Exeption("Inappropriate color");
    }
    
    private function validateOpacity(float $opacity) : float
    {
        if(0 <= $opacity || $opacity <= 1) return $opacity;
        throw new Exeption("Inappropriate opacity");
    }
    
    private function setBlue(int $blue) : void
    {
        $this->blue = $this->validateColor($blue);
    }
    
    private function setGreen(int $green) : void
    {
        $this->green = $this->validateColor($green);
    }
    
    private function setRed(int $red) : void
    {
        $this->red = $this->validateColor($red);
    }
    
    private function setOpacity(float $opacity) : void
    {
        $this->opacity = $this->validateOpacity($opacity);
    }
    
    public function getBlue() : int
    {
        return $this->blue;
    }
    
    public function getGreen() : int
    {
        return $this->green;
    }
    
    public function getRed() : int
    {
        return $this->red;
    }
    
    public function getOpacity() : float
    {
        return $this->opacity;
    }
}
?>
