FROM php:7.4.1-apache

COPY .docker/apache/apache.conf /etc/apache2/sites-available/000-default.conf
COPY html/public /var/www/html/public
#RUN apt-get update && apt-get install libpq-dev -y && docker-php-ext-install pdo_pgsql
