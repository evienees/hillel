<?php 
    require 'ColorRGB.php';
    
    $color = new ColorRGB(150, 130, 120);
    $randomColor = ColorRGB::random(true);
    
?>
<!DOCTYPE html> 
	<html>
<head>
 <style>
   body 
   {
    background-color: rgba( <?=$randomColor->getRed();?> , <?=$randomColor->getGreen();?>, <?=$randomColor->getBlue();?>,<?=$randomColor->getOpacity();?>);
    }
   #refresh {
        width: 16em;  height: 4em; font-size: 20px; 
    }
  </style>
<meta charset="UTF-8"> 
<title>Home</title>

</head>
	<body>
		<pre>
        	<h1>Красный : <?=$color->getRed()?></h1>
        	<h1>Зеленый : <?=$color->getGreen()?></h1>
        	<h1>Синий : <?=$color->getBlue()?></h1>
        	<?php 
        	$color->mix(new ColorRGB(100, 100, 100));
        	?>
        </pre>
        <pre>
        <h1>new addMixed-RGB: new ColorRGB(100, 100, 100)</h1>
        	<h1>Mixed-Красный: <?=$color->getRed()?></h1>
        	<h1>Mixed-Зеленый : <?=$color->getGreen()?></h1>
        	<h1>Mixed-Синий : <?=$color->getBlue()?></h1>
        </pre>
        <form method="get">
				<input type="submit" name="refresh" id="refresh" value="refresh">
		</form>
	</body>
</html>